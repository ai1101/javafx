package fr.eql.ai110.lambda;

import java.util.Comparator;

public class PersonneComparatorByAge implements Comparator<Personne> {

	@Override
	public int compare(Personne p1, Personne p2) {
		return p1.getAge() - p2.getAge();
	}

}
