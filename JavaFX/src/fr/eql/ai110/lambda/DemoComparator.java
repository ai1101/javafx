package fr.eql.ai110.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DemoComparator {

	public static void main(String[] args) {
		
		List<Personne> personnes = new ArrayList<Personne>();
		
		Personne harry = new Personne("Potter", "Harry", 11);
		Personne albus = new Personne("Dumbledore", "Albus", 96);
		Personne severus = new Personne("Rogue", "Severus", 35);
		
		Collections.addAll(personnes, harry, albus, severus);
		
		System.out.println("collection non triée : ");
		for (Personne personne : personnes) {
			System.out.println(personne.toString());
		}

		//méthode 1 : je crée une classe d'implémentation
		//personnes.sort(new PersonneComparatorByAge()); //trie par âge
		
		//méthode 2 : implémentation à la volée 
		/*
		personnes.sort(new Comparator<Personne>() {
			@Override
			public int compare(Personne p1, Personne p2) {
				return p1.getAge() - p2.getAge();
			}		
		});
		*/
		
		//méthode 3 : lambda expression
		personnes.sort( (p1, p2) -> p1.getAge() - p2.getAge() );
		
		System.out.println("collection triée : ");
		for (Personne personne : personnes) {
			System.out.println(personne.toString());
		}
		
		
	}

}
