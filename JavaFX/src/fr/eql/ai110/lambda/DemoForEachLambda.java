package fr.eql.ai110.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoForEachLambda {

	public static void main(String[] args) {
		
		List<String> items = new ArrayList<String>();
		Collections.addAll(items, "A","B","C","D","E");
		
		//avant java 8
		for (String string : items) {
			System.out.println("En java 7 : " + string);
		}
		
		System.out.println();
		
		//En java 8 
		items.forEach(item -> System.out.println("En java 8 : " + item));
		
		//en java 8 avec traitement plus complexe 
		items.forEach(toto -> {
			if (toto.equals("C")) {
				System.out.println("J'ai trouvé C : " + toto);
			}
		});
		
		

	}

}
