package fr.eql.ai110.javafx.composant;

import javafx.beans.binding.NumberBinding;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class DemoObservable {

	public static void main(String[] args) {
		
		//Sans observable 
		int a = 10;
		int b = 10; 
		int sum = a + b;
		System.out.println(sum);
		a = 15;
		System.out.println(sum);
		
		System.out.println("--------");
		
		//Avec observable 
		SimpleIntegerProperty obsA = new SimpleIntegerProperty(10);
		SimpleIntegerProperty obsB = new SimpleIntegerProperty(10);
		
		//On binde (relie dynamiquement) obsA et obsB : 
		NumberBinding obsSum = obsA.add(obsB);
		
		System.out.println(obsSum.getValue());
		
		obsA.set(15);
		
		System.out.println(obsSum.getValue());
		
		
	}

}
