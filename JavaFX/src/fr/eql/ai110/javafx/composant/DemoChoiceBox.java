package fr.eql.ai110.javafx.composant;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DemoChoiceBox extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox(10);
		Label label = new Label();
		
		//On instancie une choicebox (un menu déroulant) : 
		ChoiceBox<String> choiceBox = new ChoiceBox<String>();
		
		//on ajoute des valeurs selectionnables dans la choiceBox
		choiceBox.getItems().addAll("Paris", "Teheran", "Mexico", "Berlin");
		
		//par défaut, il sera sur le premier élément 
		choiceBox.getSelectionModel().select(0);
		
		root.getChildren().addAll(label, choiceBox);
		
		//j'ajoute un listener à ma choiceBox : il écoute le changement
		choiceBox.getSelectionModel().selectedItemProperty()
		.addListener(new ChangeListener<String>() {

			//la méthode changed prend en paramètre un observable (ici de String), 
			//l'ancienne valeur selectionnée et la nouvelle valeur selectionnée
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				label.setText(oldValue + " --> " + newValue); 
			}
			
		});
		
		Scene scene = new Scene(root, 400, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier ChoiceBox");
		primaryStage.sizeToScene();
		primaryStage.show();
		
	}

	public static void main(String[] args) {
		launch(args);
	}

}
