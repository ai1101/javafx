package fr.eql.ai110.javafx.composant;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DemoRadioButton extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox(10);

		//On instancie des radiobuttons : 
		RadioButton rb1 = new RadioButton("Féminin");
		RadioButton rb2 = new RadioButton("Masculin");
		RadioButton rb3 = new RadioButton("Indéterminé");

		Label label = new Label();

		//on lit entre les radiobuttons au sein d'un ToggleGroup 
		//pour ne permettre la selection que d'un seul d'entre eux à la fois

		ToggleGroup group = new ToggleGroup();
		group.getToggles().addAll(rb1,rb2,rb3);

		//on ajoute nos composant à la liste des composants enfants du panneau root
		root.getChildren().addAll(label,rb1,rb2,rb3);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-background-color : teal");

		Scene scene = new Scene(root, 550, 100);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mon premier RadioButton");
		primaryStage.sizeToScene();
		primaryStage.show();

		//on ajoute un listener à notre toggleGroup
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			//lorsqu'un radiobutton est selectionné, le text de ce radiobutton
			//est affiché dans le label
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				Labeled selectedItem = (Labeled) newValue;
				label.setText("Changement -> " + selectedItem.getText());
			}
		});


	}

	public static void main(String[] args) {
		launch(args);
	}

}

