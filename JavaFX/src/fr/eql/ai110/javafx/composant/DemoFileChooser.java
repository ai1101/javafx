package fr.eql.ai110.javafx.composant;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DemoFileChooser extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox root = new VBox(5);
		
		Button button = new Button("ouvrir");
		Label label = new Label();
		
		root.getChildren().addAll(button, label);
		
		root.setStyle("-fx-background-color:orange");
		root.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(root, 400, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier file chooser");
		primaryStage.sizeToScene();
		primaryStage.show();
		
		button.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				
				//j'instancie un file initDir : point d'entrée de la fenêtre qui permet 
				//à l'utilisateur de choisir un fichier 
				
				File initDir = new File("C://Users/fahadi/Desktop/Workspace_JFX_AI110");
				fileChooser.setInitialDirectory(initDir);
				
				//On ouvre une fenetre contextuelle pour permettre à l'utilisateur de 
				//choisir le fichier à ouvrir :
				File f = fileChooser.showOpenDialog(primaryStage.getOwner());
				
				//si l'utilisateur a bien selectionné un fichier :
				//je récupère le chemin absolu, et je l'affiche dans label 
				if (f != null) {
					label.setText(f.getAbsolutePath());
				}
				
			}
		});
		
	}

	public static void main(String[] args) {
		launch(args);
	}

}
