package fr.eql.ai110.javafx.composant;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DemoCheckBox extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		VBox root = new VBox();
		Label label = new Label("Choisissez vos personnages préférés : ");
		Label display = new Label();
		Button button = new Button("valider");

		//j'instancie une checkbox par option selectionnable
		CheckBox oberyn = new CheckBox("Oberyn Martell");
		CheckBox cersei = new CheckBox("Cersei Lannister");
		CheckBox jon = new CheckBox("Jon Snow");
		CheckBox arya = new CheckBox("Arya Stark");
		CheckBox ramsay = new CheckBox("Ramsay Snow");

		//on regroupe nos checkbox dans un tableau, pour pouvoir les traiter en une fois
		CheckBox[] boxes = { oberyn, cersei, jon, arya, ramsay };

		root.getChildren().addAll(label, display);
		root.getChildren().addAll(boxes);
		root.getChildren().addAll(button);

		//on ajoute du style à notre panneau root
		root.setStyle("-fx-background-color:lightgreen");
		root.setAlignment(Pos.CENTER_LEFT);
		root.setSpacing(10);
		root.setPadding(new Insets(20));

		Scene scene = new Scene(root, 500,500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Première CheckBox");
		primaryStage.sizeToScene();
		primaryStage.show();

		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				//on instancie un stringbuilder dans lequel on stockera les cases cochées 
				//par l'utilisateur
				StringBuilder builder = new StringBuilder();

				//on interroge chacune des checkbox pour voir si elles sont cochées ou non :
				for (CheckBox checkBox : boxes) {
					if (checkBox.isSelected()) {
						builder.append(String.format("\t%s%n",  checkBox.getText()));
						//string.format:  objet utilitaire, ici on lui dit tab (\t)
						//puis getText (s -> string) puis un saut de ligne (%n)
					}
				}

				//si aucune case n'est cochée :
				if (builder.length() == 0) {
					builder.append("Vous  n'avez rien selectionné");
				} else {
					builder.insert(0, "Vos personnages préférés sont : \n");
				}

				label.setText(builder.toString());
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}

}
