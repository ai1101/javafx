package fr.eql.ai110.javafx.tpevenement;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ButtonHandler implements EventHandler<ActionEvent>{
	
	private ColorPanel colorPanel;
	private String color;

	public ButtonHandler(ColorPanel colorPanel, String color) {
		super();
		this.colorPanel = colorPanel;
		this.color = color;
	}

	@Override
	public void handle(ActionEvent event) {
		colorPanel.setStyle("-fx-background-color:" + color);
	}

	
}
