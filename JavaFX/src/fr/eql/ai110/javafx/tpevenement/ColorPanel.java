package fr.eql.ai110.javafx.tpevenement;

import javafx.scene.layout.Pane;

public class ColorPanel extends Pane {
	
	public ColorPanel() {
		setPrefSize(350, 300);
		setStyle("-fx-background-color:yellow");
	}

}
