package fr.eql.ai110.javafx.tpevenement;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ButtonsPanel extends HBox {
	
	private Button redButton = new Button("rouge");
	private Button blueButton = new Button("bleu");
	private Button greenButton = new Button("vert");
	private Button orangeButton = new Button("orange");
	
	public ButtonsPanel() {
		getChildren().addAll(redButton, blueButton, greenButton, orangeButton);
		setSpacing(5);
		setAlignment(Pos.CENTER);
		setPadding(new Insets(5));
		
		//méthode #4 : gestion de l'evenement dans 
		//le constructeur du panneau qui contient le bouton
		redButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				//je remonte l'arborescence jusqu'au panneau root
				//à partir du panneau courant
				
				MainPanel root = (MainPanel) ButtonsPanel.this.getScene().getRoot();
				
				root.getColorPanel().setStyle("-fx-background-color:red");
			}
		});
		
	}

	public Button getRedButton() {
		return redButton;
	}

	public void setRedButton(Button redButton) {
		this.redButton = redButton;
	}

	public Button getBlueButton() {
		return blueButton;
	}

	public void setBlueButton(Button blueButton) {
		this.blueButton = blueButton;
	}

	public Button getGreenButton() {
		return greenButton;
	}

	public void setGreenButton(Button greenButton) {
		this.greenButton = greenButton;
	}

	public Button getOrangeButton() {
		return orangeButton;
	}

	public void setOrangeButton(Button orangeButton) {
		this.orangeButton = orangeButton;
	}
	
	

}
