package fr.eql.ai110.javafx.tpevenement;

import javafx.scene.layout.GridPane;

public class MainPanel extends GridPane {
	
	private ColorPanel colorPanel = new ColorPanel();
	private ButtonsPanel buttonsPanel = new ButtonsPanel();
	
	public MainPanel() {
		addColumn(0, buttonsPanel, colorPanel);
		
		//méthode #2 : gestion de l'évènement dans le panneau principal 
		buttonsPanel.getBlueButton().setOnAction(
				event -> colorPanel.setStyle("-fx-background-color:blue"));
		
		
		//méthode #3 : gestion de l'evenement à partir d'une classe d'evenement externe
		//méthode la plus réutilisable 
		ButtonHandler handler = new ButtonHandler(colorPanel, "green");
		buttonsPanel.getGreenButton().setOnAction(handler);
		
	}

	public ColorPanel getColorPanel() {
		return colorPanel;
	}

	public void setColorPanel(ColorPanel colorPanel) {
		this.colorPanel = colorPanel;
	}

	public ButtonsPanel getButtonsPanel() {
		return buttonsPanel;
	}

	public void setButtonsPanel(ButtonsPanel buttonsPanel) {
		this.buttonsPanel = buttonsPanel;
	}
	
	

}
