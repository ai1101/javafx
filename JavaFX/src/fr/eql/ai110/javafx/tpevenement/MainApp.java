package fr.eql.ai110.javafx.tpevenement;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		MainPanel root = new MainPanel();
		
		//Méthode #1 : depuis le point d'entrée de l'application
		//méthode la plus déconseillée : surcharge la classe d'application
		root.getButtonsPanel().getOrangeButton()
			.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				root.getColorPanel().setStyle("-fx-background-color:orange");
			}
		});
		
		Scene scene = new Scene(root);
		
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.setTitle("Tp evenement ");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
