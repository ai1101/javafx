package fr.eql.ai110.javafx.mvvm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CharacterDao {

	//Classe d'accès aux données (ici pour l'entité Character)
	//en général elle interagit avec l'unité de persistence des données (BDD, fichier etc ...)
	
	//méthodes typiques : CRUD 
	//getAll(), findAll() -> renvoie toutes les entités de votre unité de persistence
	//add(Character c) -> ajouter un character
	//delete(Character c) -> supprime le character
	//update (Character c) -> modifie les attributs du character 
	
	public List<Character> getAll() {
		List<Character> characters = new ArrayList<Character>();
		
		Character oberyn = new Character("Oberyn", "Martell", 37);
		Character cersei = new Character("Cersei", "Lannister", 32);
		Character arya = new Character("Arya", "Stark", 12);
		Character ramsay = new Character("Ramsay", "Snow", 23);
		
		Collections.addAll(characters, oberyn, cersei, arya, ramsay);
		
		return characters;
	}
}
