package fr.eql.ai110.javafx.mvvm;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class DemoComboBox extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		HBox root = new HBox(10);

		Button button = new Button("valider");
		Label label = new Label();

		//J'instancie un characterDao :
		CharacterDao dao = new CharacterDao();
		
		//On instancie une liste d'observable de personnages,
		//qui va être bindée à une méthode de notre dao
		ObservableList<Character> observableCharacters = 
				FXCollections.observableArrayList(dao.getAll());
		
		//On instancie une comboBox qui charge la liste d'observable :
		ComboBox<Character> cb = new ComboBox<Character>(observableCharacters);
		
		//améliorer l'affichage dans la comboBox :
		cb.setPromptText("personnages");
		
		//restreindre l'affichage : 
		cb.setVisibleRowCount(2);
		
		//On formate l'affichage de l'objet dans la comboBox : 
		cb.setConverter(new StringConverter<Character>() {
			
			@Override
			public String toString(Character object) {
				return String.format("%s %s", object.getFirstName(), object.getName());
			}
			
			@Override
			public Character fromString(String string) {
				return null;
			}
		});
		
		
		root.getChildren().addAll(cb, button, label);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-background-color:lavender");
		
		Scene scene = new Scene(root, 700, 100);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.setTitle("Premiere ComboBox");
		primaryStage.show();
		
		button.setOnAction( event -> {
			Character selectedCharacter = cb.getSelectionModel().getSelectedItem();
			label.setText(selectedCharacter.toString());
		});
		
		
	}

	public static void main(String[] args) {
		launch(args);

	}

}
