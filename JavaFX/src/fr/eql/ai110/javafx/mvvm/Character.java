package fr.eql.ai110.javafx.mvvm;

import java.util.Objects;

public class Character {
	
	private String firstName;
	private String name;
	private int age;
	
	public Character() {
		super();
	}
	public Character(String firstName, String name, int age) {
		super();
		this.firstName = firstName;
		this.name = name;
		this.age = age;
	}
	@Override
	public int hashCode() {
		return Objects.hash(age, firstName, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Character other = (Character) obj;
		return age == other.age && Objects.equals(firstName, other.firstName) && Objects.equals(name, other.name);
	}
	@Override
	public String toString() {
		return "Character [firstName=" + firstName + ", name=" + name + ", age=" + age + "]";
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	

}
