package fr.eql.ai110.javafx.mvvm;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DemoTableView extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		AnchorPane	anchorPane = new AnchorPane();
		CharacterDao dao = new CharacterDao();
		ObservableList<Character> observableCharacters = 
				FXCollections.observableArrayList(dao.getAll());
		
		//on instancie un TableView : un composant qui permet d'afficher des données
		//sous forme de tableau
		//le tableView est bindé avec la liste observableCharacters
		TableView<Character> tableView = new TableView<Character>(observableCharacters);
		
		//Paramétrer le tableView : 
			//j'instancie une colonne pour le prénom (String) de mon personnage (Character)
			//l'en-tête de cette colonne est "Prenom"
		TableColumn<Character, String> colFirstName = 
				new TableColumn<Character, String>("Prenom");
		//je transmet à la colonne colFirstName, l'attribut firstName de mon character
		colFirstName.setCellValueFactory(
				new PropertyValueFactory<Character, String>("firstName"));
		
		TableColumn<Character, String> colName = 
				new TableColumn<Character, String>("Nom");
		colName.setCellValueFactory(
				new PropertyValueFactory<Character, String>("name"));
		
		TableColumn<Character, Integer> colAge = 
				new TableColumn<Character, Integer>("Age");
		colAge.setCellValueFactory(
				new PropertyValueFactory<Character, Integer>("age"));
		
		//On donne les colonnes au tableView
		tableView.getColumns().addAll(colFirstName, colName, colAge);
		
		//ajuste la taille du tableau à son contenu:
		tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		//j'ajoute le tableView à mon anchorPane
		anchorPane.getChildren().add(tableView);
		anchorPane.setPrefSize(300, 200);
		
		//j'ancre le tableView sur les 4 cotés de mon anchorPane
		AnchorPane.setTopAnchor(tableView, 5.);
		AnchorPane.setBottomAnchor(tableView, 5.);
		AnchorPane.setRightAnchor(tableView, 5.);
		AnchorPane.setLeftAnchor(tableView, 5.);
		
		//Vbox d'affichage du character selectionné
		VBox vbox = new VBox();
		Label label1 = new Label("personnage selectionné :");
		Label label2 = new Label();
		vbox.getChildren().addAll(label1, label2);
		
		//j'ajoute du style à ma vbox :
		vbox.setAlignment(Pos.CENTER);
		label1.setPadding(new Insets(0, 70, 0, 0));
		vbox.setPrefWidth(600);
		vbox.setStyle("-fx-background-color: skyblue");
		
		root.setLeft(anchorPane);
		root.setRight(vbox);
		
		Scene scene = new Scene(root, 800, 400);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier Table View");
		primaryStage.sizeToScene();
		primaryStage.show();

		
		//gestion de l'evenement : quand l'utilisateur clique sur un personnage
		//il s'affiche dans le label à droite (dans la vbox)
		tableView.getSelectionModel().selectedItemProperty()
		.addListener(new ChangeListener<Character>() {

			@Override
			public void changed(ObservableValue<? extends Character> observable, 
					Character oldValue,
					Character newValue) {
				label2.setText(newValue.toString());
			}
		});
		
	}

	public static void main(String[] args) {
		launch(args);
	}

}
