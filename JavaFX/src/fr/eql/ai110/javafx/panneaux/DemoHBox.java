package fr.eql.ai110.javafx.panneaux;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DemoHBox extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox();
		
		Button btn1 = new Button("Valider");
		Button btn2 = new Button("Annuler");
		Button btn3 = new Button("retour");
		
		btn1.setTextFill(Color.CHOCOLATE);
		btn2.setStyle("-fx-background-color:teal");
		
		//root.getChildren().add(btn1);
		root.getChildren().addAll(btn1, btn2, btn3);
		
		Scene scene = new Scene(root, 400, 100);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier HBOX");
		primaryStage.sizeToScene();
		primaryStage.show();
		
		root.setAlignment(Pos.CENTER);
		root.setSpacing(20);
		
	}

}
