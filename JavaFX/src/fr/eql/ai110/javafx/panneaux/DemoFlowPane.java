package fr.eql.ai110.javafx.panneaux;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class DemoFlowPane extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FlowPane root = new FlowPane();
		
		Button btn1 = new Button("bouton 1");
		Button btn2 = new Button("bouton 2");
		Button btn3 = new Button("bouton 3");
		Button btn4 = new Button("bouton 4");
		Button btn5 = new Button("bouton 5");
		Button btn6 = new Button("bouton 6");
		Button btn7 = new Button("bouton 7");
		Button btn8 = new Button("bouton 8");
		
		root.getChildren().addAll(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8);
		
		root.setOrientation(Orientation.VERTICAL);
		root.setHgap(30);
		root.setVgap(80);
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier FlowPane");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
