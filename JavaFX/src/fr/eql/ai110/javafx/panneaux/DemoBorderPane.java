package fr.eql.ai110.javafx.panneaux;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class DemoBorderPane extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		
		Label lblBottom = new Label("Label d'en bas");
		Label lblRight = new Label("Label de droite");
		Button btnCenter = new Button("Bouton du centre");
		
		Pane panelTop = new Pane();
		panelTop.setPrefSize(700, 100);
		panelTop.setStyle("-fx-background-color: blue");
	
		Pane panelLeft = new Pane();
		panelLeft.setPrefSize(200, 700);
		panelLeft.setStyle("-fx-background-color: orange");
		
		//je place les éléments à différents endroits dans le borderPane :
		root.setBottom(lblBottom);
		root.setRight(lblRight);
		root.setCenter(btnCenter);
		root.setTop(panelTop);
		root.setLeft(panelLeft);
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier BorderPane");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
