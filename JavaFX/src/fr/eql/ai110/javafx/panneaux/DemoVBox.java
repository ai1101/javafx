package fr.eql.ai110.javafx.panneaux;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DemoVBox extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox root = new VBox();
		
		Button btn1 = new Button("cliquer");
		Button btn2  = new Button("ok");
		Button btn3 = new Button("annuler");
		
		root.getChildren().addAll(btn1, btn2, btn3);
		
		root.setAlignment(Pos.BASELINE_LEFT);
		root.setStyle("-fx-background-color:hotpink");
		
		Scene scene = new Scene(root, 400, 100);
		
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.setTitle("Première VBOX");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
