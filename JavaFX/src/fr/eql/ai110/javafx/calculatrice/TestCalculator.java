package fr.eql.ai110.javafx.calculatrice;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TestCalculator extends Application implements EventHandler<ActionEvent> {
	
	TextField textfield = new TextField();
	Button button = new Button("calculer");
	Label display = new Label("résultat");

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox(10);
		Scene scene = new Scene(root);
		
		primaryStage.setScene(scene);
		primaryStage.setWidth(400);
		primaryStage.setHeight(100);
		primaryStage.setTitle("Test calculator");
		
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(textfield, button, display);
		
		//le mot clé this pointe vers la classe dans laquelle on se trouve
		//ici : TestCalculator, qui implémente l'interface fonctionnelle EventHandler
		//lorsque la méthode setOnAction est appelée via button,
		//la méthode handle dont le code est implémenté ci-dessous sera executée
		button.setOnAction(this);
		
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void handle(ActionEvent event) {
		//les objets ScriptEngineManager et ScriptEngine permettent
		//d'utiliser du langage de script dans notre application. (ici javascript : js)
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		
		try {
			//Javascript possède une méthode eval, qui prend en paramètre
			//une string de type équation et renvoie le résultat du calcul
			Object result = engine.eval(textfield.getText());
			display.setText(result.toString());
			
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
