package fr.eql.ai110.javafx.calculatrice;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Calculator extends Application implements EventHandler<ActionEvent>{

	private TextField display = new TextField("0");

	private Button clear = new Button("C");

	//j'instancie un tableau de tableaux de strings 
	//chaque tableau de string correspondra à une HBox
		//chaque string correspondra à un bouton
	private String[][] buttons = {
			{"1","2","3","+"},
			{"4","5","6","-"},
			{"7","8","9","/"},
			{"+/-","0",".","*"},
			{"(",")","="}
	};
	
	private boolean lastClickEgal = true;


	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//permet de rendre le textField fermé à l'édition
		display.setEditable(false);
		
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 320, 420);

		//Hbox top : textfield et bouton clear
		HBox top = new HBox();
		top.getChildren().addAll(display, clear);
		top.setAlignment(Pos.CENTER);
		display.setPrefWidth(270);
		display.setPrefHeight(50);
		clear.setPrefSize(50, 50);
		
		//lorsque l'on clique sur le bouton clear,
		//la méthode handle de cette classe Calculator est appelée
		clear.setOnAction(this);

		//Vbox center : contient les HBox qui contiennent les boutons 
		VBox center = new VBox();
		center.setAlignment(Pos.CENTER);

		//J'itère sur le tableau String[][] :
		for (String[] strings : buttons) {
			//pour chaque tableau de strings de mon tableau buttons,
			//j'instancie une nouvelle HBox 
				//(exemple Hbox qui contient les boutons "1","2","3","+")
			HBox hbox = new HBox();

			for (String string : strings) {
				//Dans cette Hbox, pour chaque string, 
				//j'instancie un bouton (exemple : bouton "1")
				Button button = new Button(string);
				button.setStyle("-fx-font-size:25px");
				button.setPrefSize(80, 80);
				//j'ajoute le bouton à ma hbox
				hbox.getChildren().add(button);
				
				//lorsque l'utilisateur clique sur le bouton, la méthode handle 
				//de la classe dans laquelle je me trouve est appelée
				button.setOnAction(this);
			}
			//j'ajoute la hbox à ma vbox center :
			center.getChildren().add(hbox);
		}

		//je stylise les derniers boutons pour ne pas avoir de vide : 
		//1-je récupère la dernière hbox de ma vbox 
		HBox last = (HBox) center.getChildren().get( center.getChildren().size() - 1 );
		//2-je récupère le dernier bouton de cette hbox :
		Button lastButton = (Button) last.getChildren().get( last.getChildren().size() - 1 );
		//3-je lui applique un style personnalisé : 
		lastButton.setPrefWidth(160);
		lastButton.setStyle("-fx-font-size:35px");

		//j'ajoute à mon borderPane parent la HBox top et la VBox center :
		root.setTop(top);
		root.setCenter(center);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Calculator");
		primaryStage.sizeToScene();
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void handle(ActionEvent event) {
		//je récupère qui a déclenché l'évènement : le bouton 
		//sur lequel l'utilisateur vient de cliquer
		Button btnClick = (Button) event.getSource();
		
		if(lastClickEgal) { //équivalent de if (lastClickEgal == true)
			//lastClickEgal à true : 
			//j'initialise à chaîne vide la string contenue dans display
			display.setText(""); 
		}
		
		try { 
			//si le bouton cliqué est un nombre :
			Integer.parseInt(btnClick.getText());
			display.setText(display.getText() + btnClick.getText());
			lastClickEgal = false;
		} catch (Exception e) { //si le bouton cliqué est autre chose qu'un nombre,
								//on va dans le catch :	
			switch (btnClick.getText()) {
			
			//les cas où l'utilisateur clique sur un opérateur : 
			//je concatène le contenu du bouton à la string préexistante de display :
			case "+":
			case "-":
			case "/":
			case "*":
			case "(":
			case ")":
			case ".":
				display.setText(display.getText() + btnClick.getText());
				lastClickEgal = false;
				break;
			//cas où le bouton cliqué est C : 
			case "C":
				display.setText("0"); //j'inscris "0" dans le display
				lastClickEgal = true;
				break;
			//cas où le bouton est cliqué est égal :
			case "=":
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("js");
				try {
					//j'utilise la méthode eval() de JavaScript pour calculer le résultat
					Object result = engine.eval(display.getText());
					//j'inscris le résultat dans le TextField display
					display.setText(result.toString());
					lastClickEgal = true;
				} catch (ScriptException ex) {
					ex.printStackTrace();
				}
				break;
			case "+/-":
				//si le premier caractère de la chaine de caractère est "-" : je l'enlève :
				//méthode substring : on récupère la string à partir de l'index 1
				if (display.getText().indexOf("-") == 0) {
					display.setText(display.getText().substring(1));
				} else {
				//si le premier caractère n'est pas un "-", je concatène "-" 
				//puis la chaine de caractère du display
					display.setText("-" + display.getText());
				}
				break;
			default:
				break;
			}
		}
	}

}
