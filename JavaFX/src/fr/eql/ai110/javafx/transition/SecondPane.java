package fr.eql.ai110.javafx.transition;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class SecondPane extends HBox {
	
	private Label label = new Label("Deuxieme panneau");
	
	public SecondPane() {
		setStyle("-fx-background-color:hotpink");
		getChildren().add(label);
		setAlignment(Pos.CENTER);
		setPrefSize(200, 400);
	}

}
