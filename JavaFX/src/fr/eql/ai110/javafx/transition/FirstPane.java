package fr.eql.ai110.javafx.transition;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class FirstPane extends HBox {
	
	private Button button = new Button("Changer vers le second panneau");
	
	public FirstPane() {
		setStyle("-fx-background-color:orange");
		setPrefSize(400, 200);
		setAlignment(Pos.CENTER);
		
		button.setOnAction( event -> {
			//j'instancie un panneau de type SecondPane
			SecondPane secondPane = new SecondPane();
			//J'instancie une nouvelle scene avec ce panneau SecondPane
			Scene scene = new Scene(secondPane);
			
			//Je récupère la stage de mon application Javafx, en remontant l'arborescence
			//depuis le FirstPane -> sa scene -> la primaryStage de l'application
			Stage stage = (Stage) this.getScene().getWindow();
			
			//je set ma nouvelle scene (avec le secondPane) à la primaryStage de mon appli
			stage.setScene(scene);
			
		});
		
		
		getChildren().add(button);
	}

}
