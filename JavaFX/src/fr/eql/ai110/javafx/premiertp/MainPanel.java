package fr.eql.ai110.javafx.premiertp;

import javafx.scene.layout.BorderPane;

public class MainPanel extends BorderPane {
	
	private ButtonPanel buttonPanel = new ButtonPanel();
	private LabelPanel labelPanel = new LabelPanel();
	
	public MainPanel() {
		setStyle("-fx-background-color:teal");
		setTop(buttonPanel);
		setLeft(labelPanel);
	}

}
