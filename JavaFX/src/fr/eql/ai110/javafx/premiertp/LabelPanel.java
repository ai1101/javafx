package fr.eql.ai110.javafx.premiertp;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class LabelPanel extends VBox {
	
	private Label lbl1 = new Label("label 1");
	private Label lbl2 = new Label("label 2");
	private Label lbl3 = new Label("label 3");
	private Label lbl4 = new Label("label 4");
	
	public LabelPanel() {
		getChildren().addAll(lbl1, lbl2, lbl3, lbl4);
		setSpacing(50);
		setStyle("-fx-background-color:green ; -fx-border-color:black");
		setAlignment(Pos.CENTER);
		setPrefSize(100, 200);
	}

}
