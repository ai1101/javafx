package fr.eql.ai110.javafx.premiertp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		MainPanel mainPanel = new MainPanel();
		
		Scene scene = new Scene(mainPanel, 600, 400);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier panneau objet");
		primaryStage.sizeToScene();
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
