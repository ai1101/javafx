package fr.eql.ai110.javafx.evenement;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DemoEvent extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		HBox root = new HBox();
		Scene scene = new Scene(root, 400, 100);
		root.setAlignment(Pos.CENTER);
		
		Button button = new Button("Clique !");
		TextField input = new TextField();
		Label copy = new Label("vide");
		
		root.getChildren().addAll(input, copy, button);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Premier évènement");
		primaryStage.sizeToScene();
		primaryStage.show();
		
		//Traitement en cliquant sur le bouton : 
		
		/* Méthode #1 : Classe anonyme interne : 
		 * 
		button.setOnAction(new EventHandler<ActionEvent>() {	
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Coucou evenement, contenu input : " + 
			input.getText());
				copy.setText(input.getText());
				copy.setStyle("-fx-text-fill:hotpink");
			}
		});
		*/
		
		//Méthode #2 : avec une lambda expression : 
		//traitement à deux instructions :
		button.setOnAction( (event) -> {
			System.out.println("Coucou evenement, contenu input : " + 
					input.getText());
			copy.setText(input.getText());
			copy.setStyle("-fx-text-fill:hotpink");
		});
		
		/* Lambda expression traitement à une seule instruction : 
		 * button.setOnAction( event -> copy.setText(input.getText()));
		 */
		
		
	}

	public static void main(String[] args) {
		launch(args);
	}

}
