package fr.eql.ai110.javafx.style;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DemoStyleSheet extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox(20);
		
		Button btnOk = new Button("ok");
		Button btnCancel = new Button("annuler");
		Button btnAdd = new Button("ajouter");
		Button btnRemove = new Button("supprimer");
		
		root.getChildren().addAll(btnOk, btnCancel, btnAdd, btnRemove);
		root.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(root, 500, 200);
		
		//Charger la feuille de style css dans la scene :
		scene.getStylesheets().add(getClass().getResource("./stylesheet.css").toExternalForm());
		
		//Création d'une classe custom pour certains boutons : 
		btnAdd.getStyleClass().add("customButton");
		btnRemove.getStyleClass().add("customButton");
		btnRemove.setId("bigButton");
				
		primaryStage.setScene(scene);
		primaryStage.setTitle("StyleSheet");
		primaryStage.sizeToScene();
		primaryStage.show();
		
	}

	public static void main(String[] args) {
		launch(args);
	}

}
